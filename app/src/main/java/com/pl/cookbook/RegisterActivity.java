package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Konrad on 2018-03-17.
 */

public class RegisterActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void registerClick(View view) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");

        EditText email = findViewById(R.id.register_text_email);
        EditText login = findViewById(R.id.register_text_login);
        EditText password1 = findViewById(R.id.register_text_password1);
        EditText password2 = findViewById(R.id.register_text_password2);
        TextView info = findViewById(R.id.register_label_info);

        if (!password1.getText().toString().equals(password2.getText().toString())) {
            info.setText("Hasła nie są identyczne");
            return;
        }
        else
            info.setText("");

        User user = new User();
        user.setUsername(login.getText().toString());
        user.setPassword(password1.getText().toString());
        user.setEmail(email.getText().toString());

        try {
            new ServerIntegrator().userRegister(user);
        } catch (Exception e) {
            info.setText("Nie udało się zarejestrować nowego użytkownika");
        }

        Intent intent = new Intent(this, PrimaryActivity.class);
        startActivity(intent);
    }
}

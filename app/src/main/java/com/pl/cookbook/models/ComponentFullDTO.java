package com.pl.cookbook.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ComponentFullDTO {
    private Integer id;
    private String name;
    private String measure;
    private Double value;
}

package com.pl.cookbook.models;

import lombok.Data;

@Data
public class Cousel {

    private Integer id;
    private String name;
    private String description;

    public Cousel(){}
    public Cousel(String name, String description){
        this();
        this.name = name;
        this.description = description;
    }

    public String toString()
    {
        return name;
    }
}

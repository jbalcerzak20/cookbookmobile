package com.pl.cookbook.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.LocalDateTimeSerializer;

import org.joda.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class Rule implements Serializable {

    private Integer id;
    private String name;
    private String rule;
    private String difficulty;
    private String operationTime;
    private String personCount;
    private String pictureName;
    private Date created_at;
    private Date updated_at;
    private Integer owner;
    private Integer category;
    private Boolean isPrivate;
    private List<ComponentFullDTO> components;

    @Override
    public String toString() {
        return name;
    }
}

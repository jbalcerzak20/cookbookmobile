package com.pl.cookbook.models;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Comment {

    private Integer id;
    private String title;
    private String message;
    private Integer owner;
    private Integer rule;
    private Date created_at;
    private Date updated_at;
    private String login;

    @Override
    public String toString() {
        return (login != null ? login : "") + ": " + created_at + "\n " + this.getMessage();
    }
}

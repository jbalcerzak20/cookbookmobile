package com.pl.cookbook.models;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Category {

    private Integer id;
    private String name;
    private Date created_at;

    @Override
    public String toString() {
        return name;
    }
}

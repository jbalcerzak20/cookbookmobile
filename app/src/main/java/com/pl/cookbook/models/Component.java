package com.pl.cookbook.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Component {

    private Integer id;
    private String name;

    @Override
    public String toString() {
        return name;
    }
}

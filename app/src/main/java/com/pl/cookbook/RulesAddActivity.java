package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.Helper;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.Category;
import com.pl.cookbook.models.Component;
import com.pl.cookbook.models.ComponentFullDTO;
import com.pl.cookbook.models.Rule;
import com.pl.cookbook.models.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RulesAddActivity extends Activity {
    private EditText title;
    private ImageView image;
    private Spinner persons;
    private Spinner times;
    private Spinner difficulty;
    private EditText components;
    private EditText prepare;
    private Switch aSwitch;
    private User currentUser;
    private Category currentCategory;
    private AutoCompleteTextView autocomplete;
    private LinearLayout componetsAll;
    private Rule currentRule;
    private Boolean isUpdate = false;
    private String[] personsTab;
    private String[] timesTab;
    private String[] difficultyTab;
    private String[] measuresTab;
    private Component[] componentsAllFromApi;
    private Uri imageUri;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_add);

        difficultyTab = getResources().getStringArray(R.array.dificullty);
        timesTab = getResources().getStringArray(R.array.times);
        personsTab = getResources().getStringArray(R.array.persons);
        measuresTab = getResources().getStringArray(R.array.measures);
        title = findViewById(R.id.rules_add_title);
        image = findViewById(R.id.rules_add_image_preview);
        persons = findViewById(R.id.rules_add_persons);
        times = findViewById(R.id.rules_add_time);
        difficulty = findViewById(R.id.rules_add_difficulty);
        prepare = findViewById(R.id.rules_add_prepare);
        autocomplete = findViewById(R.id.rules_add_autocomplete);
        componetsAll = findViewById(R.id.rules_add_components_all);
        aSwitch = findViewById(R.id.rules_add_switch);

        try {
            componentsAllFromApi = (Component[]) new ServerIntegrator().getComponentALl().toArray();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        currentUser = (User) DataContainer.getObject("currentUser");
        currentCategory = (Category) DataContainer.getObject("currentCategory");
        currentRule = (Rule) DataContainer.getObject("currentRule");


        aSwitch.setOnClickListener(view -> {
            if (((Switch) view).isChecked()) {
                ((Switch) view).setText("Publiczny");
            } else {
                ((Switch) view).setText("Prywatny");
            }
        });


        ArrayAdapter<String> personsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, personsTab);
        ArrayAdapter<String> timesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, timesTab);
        ArrayAdapter<String> difficultyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, difficultyTab);
        ArrayAdapter<Component> compsAdapter = new ArrayAdapter<Component>(this, android.R.layout.simple_spinner_dropdown_item, componentsAllFromApi);


        persons.setAdapter(personsAdapter);
        times.setAdapter(timesAdapter);
        difficulty.setAdapter(difficultyAdapter);


        autocomplete.setAdapter(compsAdapter);
        autocomplete.setThreshold(1);
        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ComponentFullDTO component = (ComponentFullDTO) adapterView.getItemAtPosition(i);
                componetsAll.addView(generateLineForComponent(component));
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null)
            isUpdate = extras.getBoolean("isUpdate", false);

        initialIfEdit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private View generateLineForComponent(ComponentFullDTO component) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
//        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        ArrayAdapter<String> miaryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, measuresTab);

        TextView label = new TextView(this);
        label.setText(component.getName());
        label.setId(component.getId());
        LinearLayout.LayoutParams layoutParamsLabel = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsLabel.height = 60;
        layoutParamsLabel.width = 350;
        layoutParamsLabel.setMargins(20, 0, 0, 0);
        label.setLayoutParams(layoutParamsLabel);


        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.height = 60;
        layoutParams.width = 150;
        layoutParams.setMargins(20, 0, 0, 0);

        Button remove = new Button(this);
        remove.setBackground(getDrawable(R.drawable.primary_button));
        remove.setText("Usuń");
        remove.setLayoutParams(layoutParams);
        remove.setOnClickListener(view -> {
            componetsAll.removeView(linearLayout);
        });

        EditText ilosc = new EditText(this);
        ilosc.setText(component.getValue().toString());

        Spinner miarySpinner = new Spinner(this);
        miarySpinner.setAdapter(miaryAdapter);
        miarySpinner.setSelection(miaryAdapter.getPosition(component.getMeasure()));

        linearLayout.addView(ilosc);
        linearLayout.addView(miarySpinner);
        linearLayout.addView(label);
        linearLayout.addView(remove);

        return linearLayout;
    }

    public void addImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.setImageURI(data.getData());
            imageUri = data.getData();
        }
    }

    private List<ComponentFullDTO> getComponents() {
        List<ComponentFullDTO> componentList = new ArrayList<>();

        int childCount = componetsAll.getChildCount();

        for (int i = 0; i < childCount; i++) {
            LinearLayout row = (LinearLayout) componetsAll.getChildAt(i);
            double ilosc = Double.parseDouble(String.valueOf(((EditText) row.getChildAt(0)).getText()));
            String miara = ((Spinner) row.getChildAt(1)).getSelectedItem().toString();
            int idSkladnik = ((TextView) row.getChildAt(2)).getId();

            ComponentFullDTO componentFullDTO = new ComponentFullDTO();
            componentFullDTO.setId(idSkladnik);
            componentFullDTO.setMeasure(miara);
            componentFullDTO.setValue(ilosc);

            componentList.add(componentFullDTO);
        }
        return componentList;
    }

    private Component getComponentById(int id) {
        for (Component c : componentsAllFromApi) {
            if (c.getId().equals(new Integer(id)))
                return c;
        }
        return null;
    }

    public void addRule(View view) {
        if (!isUpdate) {
            Rule newRule = new Rule();
            newRule = fillRule(newRule);

            Rule rule = null;
            try {
                rule = new ServerIntegrator().createRule(newRule);
                new ServerIntegrator().fileUpload(imageUri, rule.getId(), this);
                Toast.makeText(this, "Dodano przepis do kategorii " + currentCategory.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, RulesActivity.class);
                startActivity(intent);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                Toast.makeText(this, "Nie udało się dodać przepisu", Toast.LENGTH_SHORT).show();
            }
        } else {
            currentRule = fillRule(currentRule);

            try {
                new ServerIntegrator().updateRule(currentRule);
//            FileDownloader.uploadFile(Status.Api.FILEUPLOAD(), imageUri, rule.getId(), this);
                isUpdate = false;
                Toast.makeText(this, "Zaktualizowano przepis do kategorii " + currentCategory.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, RulesActivity.class);
                startActivity(intent);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private Rule fillRule(Rule rule) {
        Bitmap drawingCache = ((BitmapDrawable) image.getDrawable()).getBitmap();
        rule.setName(title.getText().toString());
        rule.setRule(prepare.getText().toString());
        rule.setDifficulty(difficulty.getSelectedItem().toString());
        rule.setOperationTime(times.getSelectedItem().toString());
        rule.setPersonCount(persons.getSelectedItem().toString());
        rule.setOwner(currentUser.getId());
        rule.setComponents(getComponents());
        rule.setCategory(currentCategory.getId());
        rule.setIsPrivate(aSwitch.isChecked());
        return rule;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initialIfEdit() {
        if (currentRule == null)
            return;

        title.setText(currentRule.getName());
        prepare.setText(currentRule.getRule());
        persons.setSelection(Arrays.asList(personsTab).indexOf(currentRule.getPersonCount()));
        times.setSelection(Arrays.asList(timesTab).indexOf(currentRule.getOperationTime()));
        difficulty.setSelection(Arrays.asList(difficultyTab).indexOf(currentRule.getDifficulty()));
        try {
            image.setImageBitmap(Helper.getBitmapFromBytes(new ServerIntegrator().fileDownload(currentRule.getPictureName())));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        List<ComponentFullDTO> components = null;
        try {
            components = new ServerIntegrator().getComponentForRule(currentRule);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (ComponentFullDTO c : components) {
            componetsAll.addView(generateLineForComponent(c));
        }
    }
}

package com.pl.cookbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.helpers.Status;
import com.pl.cookbook.models.Category;

import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private CategoryActivity that = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        List<Category> categories = new ServerIntegrator().getCategoryAll();
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, R.layout.activity_row, categories);
        ListView lista = findViewById(R.id.category_list);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Category category = (Category) adapterView.getItemAtPosition(i);
                DataContainer.setObject("currentCategory", category);
                Intent intent = new Intent(that, RulesActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Status.selectItemMenu(this, item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Status.menuCreator(Status.MENUTYPE.CATEGORY, menu);
        return super.onPrepareOptionsMenu(menu);
    }
}

package com.pl.cookbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.helpers.Status;
import com.pl.cookbook.models.Category;
import com.pl.cookbook.models.Rule;
import com.pl.cookbook.models.User;

import java.util.List;

public class RulesActivity extends AppCompatActivity {

    private RulesActivity that = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        Category category = (Category) DataContainer.getObject("currentCategory");
        User user = (User) DataContainer.getObject("currentUser");

        List<Rule> rules = null;
        try {
            rules = new ServerIntegrator().getRuleForCategory(category);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        final RulesAdapter adapter = new RulesAdapter(this, (Rule[]) rules.toArray());
        ListView v = findViewById(R.id.rules_list);
        v.setAdapter(adapter);

        v.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Rule rule = (Rule) adapterView.getItemAtPosition(i);
                DataContainer.setObject("currentRule", rule);
                Intent intent = new Intent(that, RulesDetailActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Status.selectItemMenu(this, item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Status.menuCreator(Status.MENUTYPE.RULES, menu);
        return super.onPrepareOptionsMenu(menu);
    }
}

package com.pl.cookbook;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.Helper;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.helpers.Status;
import com.pl.cookbook.models.User;

public class PrimaryActivity extends AppCompatActivity {

    public PrimaryActivity that = this;
    private Button logowanie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ServerIntegrator.setPrimaryActivity(this);

        if (!Helper.isConnection(this)) {
            Toast.makeText(this, R.string.not_connection, Toast.LENGTH_LONG);
            return;
        }
        logowanie = findViewById(R.id.primary_button_login);

        logowanie.setOnClickListener(view -> {
            User currentUser = (User) DataContainer.getObject("currentUser");

            if (currentUser == null) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            } else {
                logowanie.setText(R.string.primary_button_login);
                DataContainer.clear();
            }
        });

        Button przepisy = findViewById(R.id.primary_button_rules);
        przepisy.setOnClickListener(view -> {
            Intent intent = new Intent(this, CategoryActivity.class);
            startActivity(intent);
        });

        Button przepisySzukaj = findViewById(R.id.primary_button_searchrules);
        przepisySzukaj.setOnClickListener(view -> {
            Intent intent = new Intent(this, RulesByComponentsActivity.class);
            startActivity(intent);
        });

        Button porady = findViewById(R.id.primary_button_counsel);
        porady.setOnClickListener(view -> {
            Intent intent = new Intent(this, CounselActivity.class);
            startActivity(intent);
        });

        Button pomoc = findViewById(R.id.primary_button_help);
        pomoc.setOnClickListener(view -> {
            System.out.println("click");
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        User currentUser = (User) DataContainer.getObject("currentUser");
        if (currentUser == null) {
            logowanie.setText(R.string.primary_button_login);
        } else {
            logowanie.setText(getString(R.string.primary_button_logout) + " (" + currentUser.getUsername() + ")");
        }
    }
}

package com.pl.cookbook;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.Status;
import com.pl.cookbook.models.User;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    public User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentUser = (User) DataContainer.getObject("currentUser");

        ImageView imageView = (ImageView) findViewById(R.id.main_image);
        imageView.setImageResource(R.drawable.mainpage);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Status.selectItemMenu(this, item))
            return true;

        return super.onOptionsItemSelected(item);
    }
}

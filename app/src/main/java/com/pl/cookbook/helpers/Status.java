package com.pl.cookbook.helpers;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;

import com.pl.cookbook.AccountActivity;
import com.pl.cookbook.CategoryActivity;
import com.pl.cookbook.LoginActivity;
import com.pl.cookbook.R;
import com.pl.cookbook.RulesAddActivity;
import com.pl.cookbook.RulesByComponentsActivity;

/**
 * Created by Konrad on 2018-03-24.
 */

public class Status {

    public static Boolean selectItemMenu(Activity activity, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_account: {
                Intent intent = new Intent(activity, AccountActivity.class);
                activity.startActivity(intent);
                return true;
            }
            case R.id.menu_main_rules: {
                Intent intent = new Intent(activity, CategoryActivity.class);
                activity.startActivity(intent);
                return true;
            }
            case R.id.menu_main_add: {
                Intent intent = new Intent(activity, RulesAddActivity.class);
                activity.startActivity(intent);
                return true;
            }
            case R.id.menu_main_logout: {
                Intent intent = new Intent(activity, LoginActivity.class);
                activity.startActivity(intent);
                return true;
            }
            case R.id.menu_main_help: {
                break;
            }
            case R.id.menu_main_rulesbycomponents: {
                Intent intent = new Intent(activity, RulesByComponentsActivity.class);
                activity.startActivity(intent);
                return true;
            }
            case 202:
                Intent intent = new Intent(activity, RulesAddActivity.class);
                activity.startActivity(intent);
                return true;
        }
        return false;
    }

    public static void menuCreator(MENUTYPE menutype, Menu menu) {
        switch (menutype) {
            case CATEGORY:
                menu.clear();
                menu.add(0, 101, 10, "Wyszukaj przepisy");
                menu.add(0, 102, 10, "Pomoc");
                if(true) {
                    menu.add(0, 103, 10, "Moje konto");
                    menu.add(0, 104, 10, "Wyloguj");
                }
                break;
            case RULES:
                menu.clear();
                menu.add(0, 201, 10, "Wyszukaj przepisy");
                menu.add(0, 202, 10, "Dodaj przepis");
                menu.add(0, 203, 10, "Pomoc");
                if(true) {
                    menu.add(0, 204, 10, "Moje konto");
                    menu.add(0, 205, 10, "Wyloguj");
                }
                break;
        }
    }

    public enum MENUTYPE {
        CATEGORY, RULES
    }
}

package com.pl.cookbook.helpers;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Konrad on 2018-03-25.
 */

public class DataContainer implements Serializable {

    private static HashMap<String, Object> data = new HashMap<>();

    public static Object getObject(String key) {
        return data.get(key);
    }

    public static void setObject(String key, Object value) {
        data.put(key, value);
    }

    public static void removeObject(String key) {
        data.remove(key);
    }

    public static void clear() {
        data.clear();
    }
}

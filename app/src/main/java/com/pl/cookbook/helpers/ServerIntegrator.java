package com.pl.cookbook.helpers;

import android.content.Context;
import android.net.Uri;

import com.pl.cookbook.PrimaryActivity;
import com.pl.cookbook.R;
import com.pl.cookbook.models.Category;
import com.pl.cookbook.models.Comment;
import com.pl.cookbook.models.Component;
import com.pl.cookbook.models.ComponentFullDTO;
import com.pl.cookbook.models.Cousel;
import com.pl.cookbook.models.Rule;
import com.pl.cookbook.models.User;

import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Konrad on 2018-08-12.
 */

public class ServerIntegrator {

    private static PrimaryActivity primaryActivity;

    private RestTemplate restTemplate;
    private final String server_url = primaryActivity.getString(R.string.host);

    public ServerIntegrator() {
        restTemplate = new RestTemplate();
    }

    public User userLogin(User user) throws Exception {
        String url = server_url + "/login";
        Map<String, Object> params = new HashMap<>();
        params.put("username", user.getEmail());
        params.put("password", user.getPassword());
        return restTemplate.postForObject(url, params, User.class);
    }

    public void userRegister(User user) throws Exception {
        String url = server_url + "/user/create";
        restTemplate.postForObject(url, user, Void.class);
    }

    public List<Category> getCategoryAll() {
        String url = server_url + "/category/all";
        return Arrays.asList(restTemplate.getForObject(url, Category[].class));
    }

    public List<Rule> getRuleForCategory(Category category) throws Exception {
        String url = server_url + "/rule/rulesByCategory/{categoryId}";
        return Arrays.asList(restTemplate.getForObject(url, Rule[].class, category.getId()));
    }

    public List<Comment> getCommentForRule(Rule rule) throws Exception {
        String url = server_url + "/comment/getCommentByRule/{ruleId}";
        return Arrays.asList(restTemplate.getForObject(url, Comment[].class, rule.getId()));
    }

    public List<ComponentFullDTO> getComponentForRule(Rule rule) throws Exception {
        String url = server_url + "/component/componentForRule/{ruleId}";
        return Arrays.asList(restTemplate.getForObject(url, ComponentFullDTO[].class, rule.getId()));
    }

    public List<Component> getComponentALl() throws Exception {
        String url = server_url + "/component/all";
        return Arrays.asList(restTemplate.getForObject(url, Component[].class));
    }

    public void updateRule(Rule rule) throws Exception {
        String url = server_url + "/rule/update";
        restTemplate.postForObject(url, rule, Void.class);
    }

    public List<Rule> findRuleByComponents(List<Integer> componentList) throws Exception {
        String url = server_url + "/rule/rulesByComponent";
        return Arrays.asList(restTemplate.postForObject(url, componentList, Rule[].class));
    }

    public List<Cousel> getCouselAll() throws Exception {
        String url = server_url + "/cousel/all";
        return Arrays.asList(restTemplate.getForObject(url, Cousel[].class));
    }

    public Cousel getCouselById(Integer id) throws Exception {
        String url = server_url + "/cousel/{couselId}";
        return restTemplate.getForObject(url, Cousel.class, id);
    }

    public Rule createRule(Rule rule) throws Exception {
        String url = server_url + "/rule/create";
        return restTemplate.postForObject(url, rule, Rule.class);
    }

    public byte[] fileDownload(String fileName) throws Exception {
        return FileDownloader.downloadFile(server_url + "/rest/file/get/", fileName);
    }

    public void fileUpload(Uri imageUri, Integer ruleId, Context context) throws Exception {
        FileDownloader.uploadFile(server_url + "/rest/file/add", imageUri, ruleId, context);
    }

    public void createComment(Comment comment) throws Exception {
        String url = server_url + "/comment/create";
        restTemplate.postForObject(url, comment, Void.class);
    }

    public void updateUser(User user) throws Exception {
        String url = server_url + "/rest/user/update";
        restTemplate.put(url, user);
    }

    public static void setPrimaryActivity(PrimaryActivity context) {
        primaryActivity = context;
    }

}

package com.pl.cookbook;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.Rule;

/**
 * Created by Konrad on 2018-03-27.
 */

public class RulesAdapter extends ArrayAdapter<Rule> {

    private Activity context;
    private Rule[] rules;

    public RulesAdapter(Activity context, Rule[] rules) {
        super(context, R.layout.activity_rules_row, rules);
        this.context = context;
        this.rules = rules;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = context.getLayoutInflater();
        View inflate = layoutInflater.inflate(R.layout.activity_rules_row, null, true);

        ImageView imageView = (ImageView) inflate.findViewById(R.id.rules_row_image);
        TextView textView = (TextView) inflate.findViewById(R.id.rules_row_label);

        textView.setText(rules[position].getName());

        try {
            byte[] bytes = new ServerIntegrator().fileDownload(rules[position].getPictureName());
            Bitmap dec = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            imageView.setImageBitmap(dec);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return inflate;
    }
}

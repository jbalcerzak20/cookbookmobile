package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.helpers.Status;
import com.pl.cookbook.models.Component;
import com.pl.cookbook.models.Rule;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Konrad on 2018-06-13.
 */

public class RulesByComponentsActivity extends AppCompatActivity {
    private MultiAutoCompleteTextView autoCompleteTextView;
    private ListView listView;
    private List<Integer> componentList;
    private RulesByComponentsActivity that = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_by_components);
        autoCompleteTextView = findViewById(R.id.rulessearch_multiautocomplete);
        componentList = new ArrayList<>();

        List<Component> componentAll = null;
        try {
            componentAll = new ServerIntegrator().getComponentALl();
        } catch (Exception e) {
            System.out.println(e);
        }
        ArrayAdapter<Component> compsAdapter = new ArrayAdapter<Component>(this, android.R.layout.simple_spinner_dropdown_item, componentAll);


        autoCompleteTextView.setAdapter(compsAdapter);
        autoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        autoCompleteTextView.setThreshold(2);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Component component = (Component) adapterView.getItemAtPosition(i);
                componentList.add(component.getId());
            }
        });

        listView = findViewById(R.id.rulessearch_show);
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Rule rule = (Rule) adapterView.getItemAtPosition(i);
            DataContainer.setObject("currentRule", rule);
            Intent intent = new Intent(that, RulesDetailActivity.class);
            startActivity(intent);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Status.selectItemMenu(this, item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    public void search(View view) {

        List<Rule> ruleByComponents = null;
        try {
            ruleByComponents = new ServerIntegrator().findRuleByComponents(componentList);
        } catch (Exception e) {
            System.out.println(e);
        }

        ArrayAdapter<Rule> ruleArrayAdapter = new ArrayAdapter<Rule>(this,android.R.layout.simple_spinner_dropdown_item,ruleByComponents);
        listView.setAdapter(ruleArrayAdapter);
        autoCompleteTextView.setText("");
        componentList.clear();
    }
}

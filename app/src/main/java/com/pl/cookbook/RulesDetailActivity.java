package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.Helper;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.Comment;
import com.pl.cookbook.models.ComponentFullDTO;
import com.pl.cookbook.models.Rule;
import com.pl.cookbook.models.User;

import java.util.List;

public class RulesDetailActivity extends Activity {
    private Rule currentRule;
    private EditText textComment;
    private User currentUser;
    private ListView comments;
    private TextView textComponent;
    private Button addComment;
    private ImageView imageView;
    private TextView title;
    private TextView times;
    private TextView rule;
    private Button editRule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_detail);

        this.currentRule = (Rule) DataContainer.getObject("currentRule");
        this.currentUser = (User) DataContainer.getObject("currentUser");

        textComponent = findViewById(R.id.rules_details_components);
        textComment = findViewById(R.id.rules_detail_text_comment);
        addComment = findViewById(R.id.rules_detail_button_addComment);
        imageView = findViewById(R.id.rules_detail_image);
        times = findViewById(R.id.rules_detail_time);
        title = findViewById(R.id.rules_detail_title);
        rule = findViewById(R.id.rules_detail_prepare);
        comments = findViewById(R.id.rules_detail_comments);

        List<ComponentFullDTO> componentForRule = null;
        try {
            componentForRule = new ServerIntegrator().getComponentForRule(currentRule);
            setTextComponent(componentForRule);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (currentUser == null) {
            ((ViewGroup) addComment.getParent()).removeView(addComment);
            ((ViewGroup) textComment.getParent()).removeView(textComment);
        }

        try {
            imageView.setImageBitmap(Helper.getBitmapFromBytes(new ServerIntegrator().fileDownload(currentRule.getPictureName())));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        times.setText(" Przepis na " + currentRule.getPersonCount() + "\n Czas przygotowania: " + currentRule.getOperationTime() + "\n Trudność: " + currentRule.getDifficulty());
        title.setText(currentRule.getName());
        rule.setText(currentRule.getRule());

        ArrayAdapter<Comment> adapter = new ArrayAdapter<>(this, R.layout.activity_comment_row, getCommentByRule());
        comments.setAdapter(adapter);

        editRule = findViewById(R.id.rules_detail_button_edit);
        if (currentUser == null || !currentRule.getOwner().equals(currentUser.getId())) {
            ((ViewGroup) editRule.getParent()).removeView(editRule);
        }
    }

    public void editRule(View view) {
        Intent intent = new Intent(this, RulesAddActivity.class);
        intent.putExtra("isUpdate", true);
        startActivity(intent);
    }

    public void addCommnet(View view) {
        Comment comment = new Comment();
        comment.setMessage(textComment.getText().toString());
        comment.setOwner(currentUser.getId());
        comment.setRule(currentRule.getId());
        try {
            new ServerIntegrator().createComment(comment);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        ArrayAdapter<Comment> commentArrayAdapter = new ArrayAdapter<>(this, R.layout.activity_comment_row, getCommentByRule());
        comments.setAdapter(commentArrayAdapter);
        commentArrayAdapter.notifyDataSetChanged();

        textComment.setText("");
    }

    public Comment[] getCommentByRule() {
        List<Comment> commentForRule = null;
        try {
            commentForRule = new ServerIntegrator().getCommentForRule(currentRule);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return (Comment[]) commentForRule.toArray();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<ComponentFullDTO> componentForRule = null;
        try {
            componentForRule = new ServerIntegrator().getComponentForRule(currentRule);
            setTextComponent(componentForRule);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            imageView.setImageBitmap(Helper.getBitmapFromBytes(new ServerIntegrator().fileDownload(currentRule.getPictureName())));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        times.setText(" Przepis na " + currentRule.getPersonCount() + "\n Czas przygotowania: " + currentRule.getOperationTime() + "\n Trudność: " + currentRule.getDifficulty());
        title.setText(currentRule.getName());
        rule.setText(currentRule.getRule());

        ArrayAdapter<Comment> adapter = new ArrayAdapter<>(this, R.layout.activity_comment_row, getCommentByRule());
        comments.setAdapter(adapter);

//        if (editRule != null && (currentUser == null || !currentRule.getOwner().equals(currentUser.getId()))) {
//            ((ViewGroup) editRule.getParent()).removeView(editRule);
//        }
    }

    private void setTextComponent(List<ComponentFullDTO> componentForRule) {
        if (componentForRule != null) {
            textComponent.setText("");
            for (ComponentFullDTO c : componentForRule) {
                textComponent.append("* " + c.getValue() + " " + c.getMeasure() + " " + c.getName() + "\n");
            }
        }
    }

    ;
}

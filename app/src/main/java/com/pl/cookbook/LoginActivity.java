package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.User;

/**
 * Created by Konrad on 2018-03-17.
 */

public class LoginActivity extends Activity {
    private User currentUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (savedInstanceState != null) {
            currentUser = (User) savedInstanceState.getSerializable("currentUser");
        }
    }

    public void loginClick(View view) {
        String email = getValueFromText(findViewById(R.id.login_text_email));
        String password = getValueFromText(findViewById(R.id.login_text_password));
        TextView info = findViewById(R.id.login_label_info);
        info.setTextColor(Color.RED);

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        User response = null;
        try {
            response = new ServerIntegrator().userLogin(user);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (response != null) {
            Intent intent = new Intent(this, PrimaryActivity.class);
            DataContainer.setObject("currentUser", response);
            info.setText("");
            startActivity(intent);

        } else {
            info.setText("Dane niepoprawne");
        }
    }

    public void registerClick(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private String getValueFromText(View view) {
        EditText text = (EditText) view;
        return text.getText().toString();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("currentUser", currentUser);
        super.onSaveInstanceState(outState);
    }
}

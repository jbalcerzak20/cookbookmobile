package com.pl.cookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.pl.cookbook.helpers.DataContainer;
import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.User;

public class AccountActivity extends Activity {

    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        EditText email = (EditText) findViewById(R.id.account_text_email);
        EditText login = (EditText) findViewById(R.id.account_text_login);
        EditText password = (EditText) findViewById(R.id.account_text_password);

        currentUser = (User) DataContainer.getObject("currentUser");
        if (currentUser != null) {
            email.setText(currentUser.getEmail());
            login.setText(currentUser.getUsername());
            password.setText(currentUser.getPassword());
        }
    }

    public void accountSaveClick(View view) {
        EditText email = (EditText) findViewById(R.id.account_text_email);
        EditText login = (EditText) findViewById(R.id.account_text_login);
        EditText password = (EditText) findViewById(R.id.account_text_password);

        currentUser.setPassword(password.getText().toString());
        currentUser.setUsername(login.getText().toString());
        currentUser.setEmail(email.getText().toString());


        try {
            new ServerIntegrator().updateUser(currentUser);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Toast.makeText(this, "Zaktualizowano użytkownika", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

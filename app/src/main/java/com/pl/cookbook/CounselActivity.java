package com.pl.cookbook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;

import com.pl.cookbook.helpers.ServerIntegrator;
import com.pl.cookbook.models.Cousel;

import java.util.List;

public class CounselActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counsel);
        List<Cousel> couselAll = null;
        try {
            couselAll = new ServerIntegrator().getCouselAll();
        } catch (Exception e) {
            System.out.println(e);
        }

        EditText descriprion = findViewById(R.id.cousel_text_view);
        descriprion.setEnabled(false);

        ArrayAdapter<Cousel> couselAdapter = new ArrayAdapter<Cousel>(this,R.layout.activity_row,couselAll);

        ListView listView = findViewById(R.id.cousel_list_view);
        listView.setAdapter(couselAdapter);

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Cousel cousel = (Cousel) adapterView.getItemAtPosition(i);
            descriprion.setText(cousel.getDescription());
        });
    }
}
